<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        App\Lesson::truncate();
        App\Tag::truncate();

        factory('App\User',1)->create();
        factory('App\Tag',10)->create();
        factory('App\Lesson',10)->create();
//        App\LessonTag::truncate();
//        factory('App\LessonTag',10)->create();
        Model::reguard();
    }
}
