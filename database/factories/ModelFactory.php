<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(123456),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Lesson::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
        'some_bool' => $faker->boolean()
    ];
});
$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});
$factory->define(App\LessonTag::class, function (Faker\Generator $faker) {
    $tag_id = App\Tag::lists('id');
    $lesson_id = App\Lesson::lists('id');

    return [
        'lesson_id' => $faker->randomElement([$lesson_id]),
        'tag_id' =>$faker->randomElement([$tag_id])
    ];
});



