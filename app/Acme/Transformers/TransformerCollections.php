<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 1/4/2016
 * Time: 2:59 PM
 */

namespace App\Acme\Transformers;


abstract class TransformerCollections
{
    /**
     * for getting all records
     * @param $lessons
     * @return array
     */
    public function transformCollection(array $item)
    {
        return array_map([$this , 'transform'], $item);
    }


    /**
     * use for tags transformation
     * @param array $item
     * @return array
     */
    public function tagsTransformCollection(array $item)
    {
        return array_map([$this , 'tagsTransform'], $item);
    }

}