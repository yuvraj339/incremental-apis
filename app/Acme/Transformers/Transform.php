<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 1/4/2016
 * Time: 3:00 PM
 */

namespace App\Acme\Transformers;


class Transform extends TransformerCollections
{
    /**
     *
     * for getting single record information and work for multiple records use with transformCollection function as i did
     * @param $lessons
     * @return array
     */
    public function transform($lessons)
    {
        return [
            'title' => $lessons['title'],
            'body' => $lessons['body'],
            'active' => (boolean) $lessons['some_bool']
        ];
    }
}