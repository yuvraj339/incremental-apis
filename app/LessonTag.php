<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonTag extends Model
{
    protected $table = 'lesson_tag';
    protected $fillable = [
        'tag_id',
        'lesson_id'
    ];
}
