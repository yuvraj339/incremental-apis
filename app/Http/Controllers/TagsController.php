<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Lesson;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Acme\Transformers\Tags;
use Illuminate\Support\Facades\Input;

class TagsController extends ApiController
{
    protected $tagsTransform;

    public function __construct(Tags $tags)
    {
        $this->tagsTransform = $tags;
    }
//    protected
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lessonId = null)
    {
        $tags = $this->getTags($lessonId);

        if($this->getTags($lessonId)!=null)
        {
            return $this->respond([
                'data' => $this->tagsTransform->tagsTransformCollection($tags->all())
            ]);
        }

        return $this->setStatusCode(404)
            ->respondWithError('Lesson not Found Exception');

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $tags = Tag::find($id);
        if (!$tags) {
            return $this->respondNotFound('sorry ! response not Found');
        }
        return $this->respond([
            'data' => $this->tagsTransform->tagsTransform($tags->toArray())
        ]);

    }

    /**
     * @param $lessonId
     * @return mixed
     */
    public function getTags($lessonId)
    {
        if(Lesson::find($lessonId) != null)
        {
            $limit = Input::get('limit') ?: 3;

            return $lessonId ? Lesson::findOrFail($lessonId)->tags : Tag::paginate($limit);
        }
    }
}
