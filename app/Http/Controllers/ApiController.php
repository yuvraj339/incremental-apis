<?php
/**
 * Created by PhpStorm.
 * User: yuvraj
 * Date: 1/4/2016
 * Time: 3:40 PM
 */

namespace app\Http\Controllers;

use Input;
use Illuminate\Contracts\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Response as errorReporting;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;


class ApiController extends Controller
{
    protected  $statusCode =errorReporting::HTTP_OK;

    /**
     * @param $statusCode
     * @return mixed
     */
    public function getStatusCode()
    {
        return  $this->statusCode;
    }

    /**
     * @param $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondNotFound($message = 'Not found')
    {
        return $this->setStatusCode(errorReporting::HTTP_BAD_REQUEST)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function internalErros($message = 'Internal errors')
    {
        return $this->setStatusCode(errorReporting::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }


    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($message)
    {return $this->respond([
        'error' => [
            'message' => $message,
            'status code' => $this->getStatusCode()
        ]
    ]);

    }

    public function respond($data , $header =[])
    {
        return response()->json($data,$this->getStatusCode(),$header);
    }

    /**
     * @param $lesson
     * @return mixed
     */
    public function respondWithPaginator(Paginator $lesson, $data)
    {
        $data = array_merge([$data,
            'paginator' => [
                'total_count' => $lesson->total(),
                'total_pages' => ceil($lesson->total() / $lesson->perPage()),
                'current_page' => $lesson->currentPage(),
                'limit' => $lesson->perPage()
            ]
        ]);
        return $this->respond($data);
    }

}