<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Acme\Transformers\Transform;
use Illuminate\Support\Facades\Input;


class LessonsController extends ApiController
{
    /**
     *
     * @var
     * Acme\Transformers\TransformCollection;
     */
    protected $lessonTransformer;

    function __construct(Transform $lessonTransformer)
    {

        $this->lessonTransformer = $lessonTransformer;
//        $this->middleware('auth.basic',['only'=>'store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = Input::get('limit') ?: 3;
        $lesson = Lesson::paginate($limit);
//        dd(get_class_methods($lesson));
        return $this->respondWithPaginator($lesson ,$data=[
            'data' => $this->lessonTransformer->transformCollection($lesson->all())
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(! Input::get('title') or ! Input::get('body'))
        {
            return  $this->setStatusCode(422)
                         ->respondWithError('Parameter Failed validation for a lesson');
        }
        Lesson::create(Input::all());
        return $this->repondCreated('Success ! Your lesson has been created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lesson = Lesson::find($id);
        if (!$lesson) {
        return $this->respondNotFound('sorry ! response not Found');
        }
        return $this->respond([
            'data' => $this->lessonTransformer->transform($lesson->toArray())
        ]);

    }


    /**
     * @param $message
     * @return mixed
     */
    public function repondCreated($message)
    {
        return $this->setStatusCode(201)
            ->respond($message);
    }

}
